/*************************************************************************
 * Name:         Tony Chen
 * Email:        tonychen@finenet.com.tw
 *
 * Compilation:  javac Point.java
 * Execution:
 * Dependencies: StdDraw.java
 *
 * Description: An immutable data type for points in the plane.
 *
 *************************************************************************/

import java.util.Comparator;

public class Point implements Comparable<Point> {

    /* !!debug for performance */ public static int cnt1 = 0;
    /* !!debug for performance */ public static int cnt2 = 0;

    // compare points by slope
    public final Comparator<Point> SLOPE_ORDER = new BySlope();

    private class BySlope implements Comparator<Point> {
        public int compare(Point p1, Point p2) {
            /* !!debug for performance */ cnt2++;
            double slope1 = slopeTo(p1);
            double slope2 = slopeTo(p2);
            return Double.compare(slope1, slope2);
        }
    }

    private final int x;                              // x coordinate
    private final int y;                              // y coordinate

    // create the point (x, y)
    public Point(int x, int y) {
        /* DO NOT MODIFY */
        this.x = x;
        this.y = y;
    }

    // plot this point to standard drawing
    public void draw() {
        /* DO NOT MODIFY */
        StdDraw.point(x, y);
    }

    // draw line between this point and that point to standard drawing
    public void drawTo(Point that) {
        /* DO NOT MODIFY */
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    // slope between this point and that point
    public double slopeTo(Point that) {
        /* !!debug for performance */ cnt1++;
        int dx = that.x - this.x;
        int dy = that.y - this.y;

        if (dy == 0) {
            if (dx == 0) return Double.NEGATIVE_INFINITY;   // myself
            return 0;                                       // horizontal line
        }

        if (dx == 0) return Double.POSITIVE_INFINITY;       // vertical line
        return (double) dy / (double) dx;
    }

    // is this point lexicographically smaller than that one?
    // comparing y-coordinates and breaking ties by x-coordinates
    public int compareTo(Point that) {
        int dx = this.y - that.y;
        if (dx != 0) return dx;
        return this.x - that.x;
    }

    // return string representation of this point
    public String toString() {
        /* DO NOT MODIFY */
        return "(" + x + ", " + y + ")";
    }

    // unit test
    public static void main(String[] args) {
        /* YOUR CODE HERE */
    }
}
