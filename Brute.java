/*************************************************************************
 * Name:         Tony Chen
 * Email:        tonychen@finenet.com.tw
 *
 * Compilation:  javac Brute.java
 * Execution:
 * Dependencies: Point.java
 *
 * Description: examines 4 points at a time and checks whether they all lie
 *  on the same line segment, printing out any such line segments to standard
 *  output and drawing them using standard drawing. To check whether the 4
 *  points p, q, r, and s are collinear, check whether the slopes between p
 *  and q, between p and r, and between p and s are all equal.
 *
 *************************************************************************/

import java.util.Arrays;

public class Brute {

    private static Point[] P;
    private static int N;
    private static boolean drawing = true;
    private static boolean drawingFixed = false;

    // created by patternRecognition(),
    // and used by recursive routine checkNext()
    private static Point[] pp;
    private static double slope;

    // m: start from index of P[]
    // level: depth for pp[]
    private static void checkNext(int m, int level) {
        int last = N - pp.length + level;
        // check pp[m..last]
        for (int k = m; k <= last; k++) {
            if (pp[0].slopeTo(P[k]) == slope) {
                pp[level] = P[k];
                if (level+1 < pp.length) {
                    checkNext(k+1, level+1);
                } else {
                    // Output pp[] list
                    StdOut.print(pp[0]);
                    for (int i = 1; i <= level; i++) {
                        StdOut.print(" -> "+pp[i]);
                    }
                    StdOut.println("");
                    if (drawing) pp[0].drawTo(pp[level]);
                }
            }
        }
    }

    // m: at least points of collinear
    private static void patternRecognition(int m) {
        if (m < 3)
            throw new java.lang.IllegalArgumentException("parameter < 3");
        if (m > N) return;

        Arrays.sort(P);

        pp = new Point[m];
        for (int i = 0; i < N-m+1; i++) {
            pp[0] = P[i];
            for (int j = i+1; j < N-m+2; j++) {
                pp[1] = P[j];
                slope = pp[0].slopeTo(pp[1]);
                checkNext(j+1, 2);
            }
        }
        pp = null;
    }

    // unit test
    public static void main(String[] args) {
        In in = new In(args[0]);    // input file
        if (args.length > 1) {
            if (args[1].equals("0")) {
                drawing = false;
            } else if (args[1].equals("1")) {
                drawingFixed = true;
            }
        }

        N = in.readInt();           // N points
        P = new Point[N];

        int minV = Integer.MAX_VALUE;
        int maxV = Integer.MIN_VALUE;

        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            P[i] = new Point(x, y);

            if (drawingFixed) {
                if (x < minV) minV = x;
                else if (x > maxV) maxV = x;

                if (y < minV) minV = y;
                else if (y > maxV) maxV = y;
            }
        }

        if (drawing) {
            // StdDraw.clear();
            if (drawingFixed) {
                minV = (minV - 1000) / 1000 * 1000;
                maxV = (maxV + 1000) / 1000 * 1000;
                StdDraw.setScale(minV, maxV);
            } else {
                StdDraw.setXscale(0, 32768);
                StdDraw.setYscale(0, 32768);
            }
            StdDraw.show(0);
            StdDraw.setPenColor(StdDraw.BLUE);

            double r = 0.001;
            if (drawingFixed) {
                r = StdDraw.getPenRadius();
                StdDraw.setPenRadius(0.015);
            }

            for (int i = 0; i < N; i++) {
                P[i].draw();
            }

            if (drawingFixed) {
                StdDraw.setPenRadius(r);
            }
        }

        patternRecognition(4);

        if (drawing) StdDraw.show(0);
    }
}
