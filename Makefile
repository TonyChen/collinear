# Makefile
# vim: set noet ts=8 sw=8:

SOURCE=Brute.java Fast.java Point.java
TARGET=$(SOURCE:.java=.class)

.PHONY: all compile clean work test help checkstyle findbugs zip generate grid

%.class: %.java
	@/bin/echo -n "% "
	javac-algs4 $<

compile: $(TARGET)
	@echo "Compiling done."

all: test work
	@echo "All done."

work: checkstyle findbugs zip

clean:
	rm -f *.class *.zip

checkstyle: $(SOURCE)
	@/bin/echo -n "% "
	checkstyle-algs4 $^

findbugs: $(SOURCE:.java=.class)
	@/bin/echo -n "% "
	findbugs-algs4 *.class

help:
	@echo "usage: make [all | checkstyle | findbugs | zip | test]"

zip: collinear.zip

generate: Generate.class

plotter: PointPlotter.class

collinear.zip: $(SOURCE)
	@/bin/echo -n "% "
	zip -u $@ $^

brute: Brute.class Point.class
	@/bin/echo -n "% "
	java-algs4 Brute data/input6.txt 1

fast: Fast.class Point.class
	@/bin/echo -n "% "
	java-algs4 Fast data/input6.txt 1
	@/bin/echo -n "% "
	java-algs4 Fast data/input8.txt 1

grid: Fast.class Point.class
	@/bin/echo -n "% "
	time java-algs4 Fast data/128x4-grid.txt 0 | tail -1
	@/bin/echo -n "% "
	time java-algs4 Fast data/256x4-grid.txt 0 | tail -1
	@/bin/echo -n "% "
	time java-algs4 Fast data/512x4-grid.txt 0 | tail -1
	@/bin/echo -n "% "
	time java-algs4 Fast data/64x8-grid.txt 0 | tail -1
	@/bin/echo -n "% "
	time java-algs4 Fast data/128x8-grid.txt 0 | tail -1
	@/bin/echo -n "% "
	time java-algs4 Fast data/256x8-grid.txt 0 | tail -1
	@/bin/echo -n "% "
	time java-algs4 Fast data/512x8-grid.txt 0 | tail -1

test: $(TARGET)
	@/bin/echo -n "% "
	java-algs4 Brute data/input6.txt 1
	@/bin/echo -n "% "
	java-algs4 Brute data/input8.txt 1
	@/bin/echo -n "% "
	java-algs4 Fast data/input6.txt 1
	@/bin/echo -n "% "
	java-algs4 Fast data/input8.txt 1
