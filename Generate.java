
public class Generate {

    // generate N points in N x N grid
    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        if (args.length > 1) {
            // C x N points on an C x N grid
            int C = Integer.parseInt(args[1]);
            StdOut.println(C*N);
            for (int i = 0; i < 32768; i += 32768/N) {
                for (int j = 0; j < 32768; j += 32768/C) {
                    StdOut.println(String.format("%5d   %5d", i, j));
                }
            }
        } else {
            StdOut.println(N);
            for (int i = 0; i < N; i++) {
                int x = StdRandom.uniform(32768);
                int y = StdRandom.uniform(32768);
                StdOut.println(String.format("%5d   %5d", x, y));
            }
        }
    }
}
