/*************************************************************************
 * Name:         Tony Chen
 * Email:        tonychen@finenet.com.tw
 *
 * Compilation:  javac Fast.java
 * Execution:    java Fast input6.txt {0|1}
 *                  0: disable drawing
 *                  1: fixed drawing (big blue point and auto-scaled)
 * Dependencies: Point.java
 *
 * Description:  A faster, sorting-based solution. Remarkably, it is possible
 *  to solve the problem much faster than the brute-force solution described
 *  above. Given a point p, the following method determines whether p
 *  participates in a set of 4 or more collinear points.
 *
 *  1. Think of p as the origin.
 *  2. For each other point q, determine the slope it makes with p.
 *  3. Sort the points according to the slopes they makes with p.
 *  4. Check if any 3 (or more) adjacent points in the sorted order have equal
 *     slopes with respect to p. If so, these points, together with p, are
 *     collinear.
 *
 *************************************************************************/

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Fast {

    private static Point[] P;
    private static int N;
    private static int criteriaHash = 5000;   // criterial size for hashCollect

    private static boolean drawing = true;
    private static boolean drawingFixed = false;

    // created by findCollinear() and
    // used by addCollinear()
    private static HashMap<Double, HashSet<Point>> collinear;

    // if Points(p,pp[from..to-1]) exists in collinear[slope] then return false
    // add Points(p,pp[from..to-1]) to collinear[slope] and return true,
    private static boolean addCollinear(double slope, Point p,
                                        Point[] pp, int from, int to) {
        HashSet<Point> set = collinear.get(slope);
        if (set != null) {
            if (set.contains(p)) return false;
            for (int i = from; i < to; i++) {
                if (set.contains(pp[i])) return false;
            }
        } else {
            // create new set() and add it to collinear[slope]
            set = new HashSet<Point>();
            collinear.put(slope, set);
        }

        // add (p, pp[from..to-1]) to set(...)
        set.add(p);
        for (int i = from; i < to; i++) {
            set.add(pp[i]);
        }

        return true;
    }

    // Collect list by slopes of p0 relative to every P[from..from+size-1],
    // then output every list which has points more than 3.
    private static void byHash(Point p0, int from, int size, int minSize) {
        HashMap<Double, ArrayList<Point>> hash =
            new HashMap<Double, ArrayList<Point>>(2*size);

        // map slopes to HashMap hash[slope] -> list(p1,p2,...)
        for (int i = 0; i < size; i++) {
            double slope = p0.slopeTo(P[from+i]);

            ArrayList<Point> xList = hash.get(slope);
            if (xList == null) {
                xList = new ArrayList<Point>();
                hash.put(slope, xList);
            }

            // add P[from+i] to xList()
            xList.add(P[from+i]);
        }

        // output each list of hash[] if it's size >= minSize
        for (Map.Entry<Double, ArrayList<Point>> entry : hash.entrySet()) {
            ArrayList<Point> xList = entry.getValue();
            int nSize = xList.size();
            // the list not include p0, and each list belongs to one slope only
            if (nSize >= minSize) {
                double slope = entry.getKey();
                Point[] pp = xList.toArray(new Point[0]);

                if (addCollinear(slope, p0, pp, 0, nSize)) {
                    // pp[] <- P[]: already sorted by (y->x)
                    // Arrays.sort(pp);

                    // p0, pp[...]
                    StdOut.print(p0);
                    for (int i = 0; i < nSize; i++) {
                        StdOut.print(" -> "+pp[i]);
                    }
                    StdOut.println("");

                    if (drawing) {
                        p0.drawTo(pp[nSize-1]);
                        StdDraw.show(0);
                    }
                }
            }
        }
    }

    // 1. Distribute slopes to p0 into buckets
    // 2. Ignore buckets which less than 3 points
    // 3. Sort for buckets and append to pp[]
    // 4. Check if any 3 (or more) adjacent points in pp[] have equal slopes
    //    with respect to p0. If so, these points, together with p0, are
    //    collinear.
    private static void byBucket(Point p0, int from, int size, int minSize) {
        int bucketSize = 16 * size;
        bucketSize = (bucketSize + 63) & ~63;
        if (bucketSize > 32*1024) {
            bucketSize /= 2;
            if (bucketSize > 32*1024) bucketSize /= 2;
        }

        ArrayList<Point>[] buckets =
            (ArrayList<Point>[]) new ArrayList[bucketSize+1];

        // distribute slopes into buckets[]
        // each buckets[] may contains more than one slopes
        for (int i = 0; i < size; i++) {
            double slope = p0.slopeTo(P[from+i]);
            // distribute slope into buckets[]
            if (slope < 0) {
                if (slope < -1) {
                    slope = -0.25 / slope;      // [-∞ ~ -1) -> [0.00 ~ 0.25)
                } else {
                    slope = 0.5 + slope / 4;    // [-1 ~  0) -> [0.25 ~ 0.50)
                }
            } else {
                if (slope < 1) {
                    slope = 0.50 + slope / 4;   // [ 0 ~ +1) -> [0.50 ~ 0.75)
                } else {
                    slope = 1 - 0.25 / slope;   // [+1 ~ +∞] -> [0.75 ~ 1.00]
                }
            }

            // slope = [0.00 ~ 1.00]
            int n = (int) (bucketSize * slope);
            ArrayList<Point> xList = buckets[n];
            if (xList == null) {
                xList = new ArrayList<Point>();
                buckets[n] = xList;
            }
            xList.add(P[from+i]);
        }

        // sort for each bucket[] and append to pp[]
        Point[] pp = new Point[size];
        int num = 0;
        for (int i = 0; i < buckets.length; i++) {
            ArrayList<Point> xList = buckets[i];
            if (xList != null && xList.size() >= minSize) {
                int start = num;
                Iterator<Point> itPoint = xList.iterator();
                while (itPoint.hasNext()) {
                    pp[num++] = itPoint.next();
                }

                // /* !!debug */
                // StdOut.print("Bucket[]: ");
                // for (int j = start; j < num; j++) {
                //     StdOut.print(pp[j]+" ");
                // }
                // StdOut.println("");

                // new points in pp[start..num-1]
                // may be more than one collinears (different slopes)
                Arrays.sort(pp, start, num, p0.SLOPE_ORDER);
            }
        }

        if (num < minSize) return;

        // /* !!debug */
        // StdOut.print("\n>>> "+p0);
        // for (int j = 0; j < num; j++) {
        //     StdOut.print(String.format(" -> %s:%g",
        //         pp[j].toString(), p0.slopeTo(pp[j])));
        // }
        // StdOut.println("");

        int k = 0;
        double kSlope = p0.slopeTo(pp[k]);
        double jSlope = Double.NEGATIVE_INFINITY;

        // Locate the same slope of three or more adjacent points
        while (k <= num-minSize) {  // minSize == m-1: exclude p0
            int j = k + 1;
            while (j < num) {
                jSlope = p0.slopeTo(pp[j]);
                if (jSlope != kSlope) break;
                j++;
            }

            // total (j-k+1) points: p0, pp[k..j-1]
            if (j-k >= minSize && addCollinear(kSlope, p0, pp, k, j)) {
                // p0, pp[k..j-1]
                StdOut.print(p0);
                for (int n = k; n < j; n++) {
                    StdOut.print(" -> "+pp[n]);
                }
                StdOut.println("");
                if (drawing) {
                    p0.drawTo(pp[j-1]);
                    StdDraw.show(0);
                }
            }

            k = j;
            kSlope = jSlope;
        }
    }

    // m: at least points of collinear
    private static void findCollinear(int m) {
        if (m < 3) {
            throw new java.lang.IllegalArgumentException(
                "collinear needs at least 3 points");
        }
        if (m > N) return;

        Arrays.sort(P);
        collinear = new HashMap<Double, HashSet<Point>>();

        int m1 = m - 1;
        for (int i = 0; i < N-m1; i++) {
            Point p0 = P[i];
            int size = N - i - 1;

            if (size <= criteriaHash) {
                byHash(p0, i+1, size, m1);
            } else {
                // big data to use byBucket()
                byBucket(p0, i+1, size, m1);
            }
        }

        /* !!debug for performance */
        StdOut.println(String.format("%d\t%d\t%d\t%d",
            N, Point.cnt1-2*Point.cnt2, Point.cnt2, collinear.size()));

        collinear = null;
    }

    // unit test
    public static void main(String[] args) {
        In in = new In(args[0]);    // input file
        if (args.length >= 2) {
            switch(args[1].charAt(0)) {
                case '0':
                    drawing = false;
                    break;
                case '1':
                    drawingFixed = true;
                    break;
                default:
                    break;
            }
            if (args.length >= 3) criteriaHash = Integer.parseInt(args[2]);
        }

        N = in.readInt();           // N points
        P = new Point[N];

        int minV = Integer.MAX_VALUE;
        int maxV = Integer.MIN_VALUE;

        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            P[i] = new Point(x, y);

            if (drawingFixed) {
                if (x < minV) minV = x;
                else if (x > maxV) maxV = x;

                if (y < minV) minV = y;
                else if (y > maxV) maxV = y;
            }
        }

        if (drawing) {
            // StdDraw.clear();
            if (drawingFixed) {
                int bw = (maxV - minV) / 32;
                minV = (minV - bw) / bw * bw;
                maxV = (maxV + bw) / bw * bw;
                StdDraw.setScale(minV, maxV);
            } else {
                StdDraw.setXscale(0, 32768);
                StdDraw.setYscale(0, 32768);
            }
            StdDraw.show(0);
            StdDraw.setPenColor(StdDraw.BLUE);

            double r = 0.001;
            if (drawingFixed) {
                r = StdDraw.getPenRadius();
                StdDraw.setPenRadius(0.015);
            }

            for (int i = 0; i < N; i++) {
                P[i].draw();
            }

            if (drawingFixed) {
                StdDraw.setPenRadius(r);
            }
        }

        findCollinear(4);

        if (drawing) StdDraw.show(0);
    }
}
